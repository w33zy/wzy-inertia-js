<?php
/**
 * Plugin Name:     wzy InertiaJS
 * Plugin URI:      https://wzymedia.com
 * Description:     A boilerplate for a simple single file plugin
 * Author:          w33zy
 * Author URI:      https://wzymedia.com
 * Text Domain:     wzy-media
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         wzyInertiaJS
 */

class wzyInertiaJS {

	public static function start(): void {

		static $started = false;

		if ( ! $started ) {
			self::add_filters();
			self::add_actions();
			self::add_shortcodes();

			$started = true;
		}
	}

	public static function add_filters(): void {}

	public static function add_actions(): void {
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'load_scripts' ) );
	}

	public static function add_shortcodes(): void {
		add_shortcode( 'wzy-inertia-html', array( __CLASS__, 'shortcode_function' ) );
	}

	public static function load_scripts(): void {
		wp_enqueue_script( 'wzy-inertia-js', plugins_url( 'assets/js/dist/wzy-inertia.js', __FILE__ ), [], '1.0.0', true );
		wp_enqueue_style( 'wzy-inertia-csq', plugins_url( 'assets/js/dist/wzy-inertia.css', __FILE__ ), [], '1.0.0' );
	}

	public static function shortcode_function( $atts ): string {
		return '<div id="wzy-turbo-js"></div>';
	}

}

wzyInertiaJS::start();
