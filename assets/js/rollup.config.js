import path from 'path'
import resolve from 'rollup-plugin-node-resolve'
import svelte from 'rollup-plugin-svelte'
import postcss from 'rollup-plugin-postcss'
import sveltePreprocess from 'svelte-preprocess'
import { terser } from 'rollup-plugin-terser'

const production = !process.env.ROLLUP_WATCH

export default [
    {
        input: 'src/index.js',
        output: {
            file: 'dist/wzy-inertia.js',
						name: 'wzyTurboJS',
            format: 'iife',
            sourcemap: !production,
        },
        context: 'window',
        plugins: [
            svelte({
            	include: './src/components/**/*.svelte',
							compilerOptions: {
								dev: !production
							},
							preprocess: sveltePreprocess({
								sourceMap: !production,
								postcss: {
									plugins: [
										require('tailwindcss')(),
										require('autoprefixer')()
									],
								},
							}),
            }),
            postcss({
							extract: path.resolve('dist/wzy-inertia.css'),
							minimize: !production
						}),
            resolve({ browser: true }),
            production && terser(),
        ],
        emitCss: true,
    },
];
