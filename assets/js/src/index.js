import App from './components/App.svelte';

let wzyTurboJS
let anchor = document.getElementById('wzy-turbo-js')

if ( anchor ) {
	wzyTurboJS = new App({
		target: anchor,
		props: {
			user: {
				identifier: '2022-442343'
			}
		}
	})
}

export default wzyTurboJS
