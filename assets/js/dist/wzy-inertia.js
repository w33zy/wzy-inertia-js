var wzyTurboJS = (function () {
    'use strict';

    function noop() { }
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }
    function create_slot(definition, ctx, $$scope, fn) {
        if (definition) {
            const slot_ctx = get_slot_context(definition, ctx, $$scope, fn);
            return definition[0](slot_ctx);
        }
    }
    function get_slot_context(definition, ctx, $$scope, fn) {
        return definition[1] && fn
            ? assign($$scope.ctx.slice(), definition[1](fn(ctx)))
            : $$scope.ctx;
    }
    function get_slot_changes(definition, $$scope, dirty, fn) {
        if (definition[2] && fn) {
            const lets = definition[2](fn(dirty));
            if ($$scope.dirty === undefined) {
                return lets;
            }
            if (typeof lets === 'object') {
                const merged = [];
                const len = Math.max($$scope.dirty.length, lets.length);
                for (let i = 0; i < len; i += 1) {
                    merged[i] = $$scope.dirty[i] | lets[i];
                }
                return merged;
            }
            return $$scope.dirty | lets;
        }
        return $$scope.dirty;
    }
    function update_slot_base(slot, slot_definition, ctx, $$scope, slot_changes, get_slot_context_fn) {
        if (slot_changes) {
            const slot_context = get_slot_context(slot_definition, ctx, $$scope, get_slot_context_fn);
            slot.p(slot_context, slot_changes);
        }
    }
    function get_all_dirty_from_scope($$scope) {
        if ($$scope.ctx.length > 32) {
            const dirty = [];
            const length = $$scope.ctx.length / 32;
            for (let i = 0; i < length; i++) {
                dirty[i] = -1;
            }
            return dirty;
        }
        return -1;
    }
    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function element(name) {
        return document.createElement(name);
    }
    function svg_element(name) {
        return document.createElementNS('http://www.w3.org/2000/svg', name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function prevent_default(fn) {
        return function (event) {
            event.preventDefault();
            // @ts-ignore
            return fn.call(this, event);
        };
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        input.value = value == null ? '' : value;
    }
    function custom_event(type, detail, { bubbles = false, cancelable = false } = {}) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, bubbles, cancelable, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error('Function called outside component initialization');
        return current_component;
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail, { cancelable = false } = {}) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail, { cancelable });
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
                return !event.defaultPrevented;
            }
            return true;
        };
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    // flush() calls callbacks in this order:
    // 1. All beforeUpdate callbacks, in order: parents before children
    // 2. All bind:this callbacks, in reverse order: children before parents.
    // 3. All afterUpdate callbacks, in order: parents before children. EXCEPT
    //    for afterUpdates called during the initial onMount, which are called in
    //    reverse order: children before parents.
    // Since callbacks might update component values, which could trigger another
    // call to flush(), the following steps guard against this:
    // 1. During beforeUpdate, any updated components will be added to the
    //    dirty_components array and will cause a reentrant call to flush(). Because
    //    the flush index is kept outside the function, the reentrant call will pick
    //    up where the earlier call left off and go through all dirty components. The
    //    current_component value is saved and restored so that the reentrant call will
    //    not interfere with the "parent" flush() call.
    // 2. bind:this callbacks cannot trigger new flush() calls.
    // 3. During afterUpdate, any updated components will NOT have their afterUpdate
    //    callback called a second time; the seen_callbacks set, outside the flush()
    //    function, guarantees this behavior.
    const seen_callbacks = new Set();
    let flushidx = 0; // Do *not* move this inside the flush() function
    function flush() {
        const saved_component = current_component;
        do {
            // first, call beforeUpdate functions
            // and update components
            while (flushidx < dirty_components.length) {
                const component = dirty_components[flushidx];
                flushidx++;
                set_current_component(component);
                update(component.$$);
            }
            set_current_component(null);
            dirty_components.length = 0;
            flushidx = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        seen_callbacks.clear();
        set_current_component(saved_component);
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
        else if (callback) {
            callback();
        }
    }
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor, customElement) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        if (!customElement) {
            // onMount happens before the initial afterUpdate
            add_render_callback(() => {
                const new_on_destroy = on_mount.map(run).filter(is_function);
                if (on_destroy) {
                    on_destroy.push(...new_on_destroy);
                }
                else {
                    // Edge case - component was destroyed immediately,
                    // most likely as a result of a binding initialising
                    run_all(new_on_destroy);
                }
                component.$$.on_mount = [];
            });
        }
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, append_styles, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            on_disconnect: [],
            before_update: [],
            after_update: [],
            context: new Map(options.context || (parent_component ? parent_component.$$.context : [])),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false,
            root: options.target || parent_component.$$.root
        };
        append_styles && append_styles($$.root);
        let ready = false;
        $$.ctx = instance
            ? instance(component, options.props || {}, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor, options.customElement);
            flush();
        }
        set_current_component(parent_component);
    }
    /**
     * Base class for Svelte components. Used when dev=false.
     */
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set($$props) {
            if (this.$$set && !is_empty($$props)) {
                this.$$.skip_bound = true;
                this.$$set($$props);
                this.$$.skip_bound = false;
            }
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.49.0' }, detail), { bubbles: true }));
    }
    function append_dev(target, node) {
        dispatch_dev('SvelteDOMInsert', { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev('SvelteDOMInsert', { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev('SvelteDOMRemove', { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ['capture'] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev('SvelteDOMAddEventListener', { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev('SvelteDOMRemoveEventListener', { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev('SvelteDOMRemoveAttribute', { node, attribute });
        else
            dispatch_dev('SvelteDOMSetAttribute', { node, attribute, value });
    }
    function prop_dev(node, property, value) {
        node[property] = value;
        dispatch_dev('SvelteDOMSetProperty', { node, property, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.wholeText === data)
            return;
        dispatch_dev('SvelteDOMSetData', { node: text, data });
        text.data = data;
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    /**
     * Base class for Svelte components with some minor dev-enhancements. Used when dev=true.
     */
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error("'target' is a required option");
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn('Component was already destroyed'); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    /* src\components\shared\Navbar.svelte generated by Svelte v3.49.0 */

    const file$4 = "src\\components\\shared\\Navbar.svelte";

    function create_fragment$4(ctx) {
    	let div2;
    	let div0;
    	let p;
    	let t0;
    	let t1;
    	let div1;
    	let ul1;
    	let li2;
    	let a0;
    	let t2;
    	let svg;
    	let path;
    	let t3;
    	let ul0;
    	let li0;
    	let a1;
    	let t5;
    	let li1;
    	let a2;
    	let t7;
    	let li3;
    	let a3;

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div0 = element("div");
    			p = element("p");
    			t0 = text(/*identifier*/ ctx[0]);
    			t1 = space();
    			div1 = element("div");
    			ul1 = element("ul");
    			li2 = element("li");
    			a0 = element("a");
    			t2 = text("Profile\n\t\t\t\t\t");
    			svg = svg_element("svg");
    			path = svg_element("path");
    			t3 = space();
    			ul0 = element("ul");
    			li0 = element("li");
    			a1 = element("a");
    			a1.textContent = "Notifications";
    			t5 = space();
    			li1 = element("li");
    			a2 = element("a");
    			a2.textContent = "Settings";
    			t7 = space();
    			li3 = element("li");
    			a3 = element("a");
    			a3.textContent = "Logout";
    			attr_dev(p, "class", "font-semibold normal-case text-xl");
    			add_location(p, file$4, 6, 3, 158);
    			attr_dev(div0, "class", "flex-1");
    			add_location(div0, file$4, 5, 1, 134);
    			attr_dev(path, "d", "M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z");
    			add_location(path, file$4, 13, 109, 471);
    			attr_dev(svg, "class", "fill-current");
    			attr_dev(svg, "xmlns", "http://www.w3.org/2000/svg");
    			attr_dev(svg, "width", "20");
    			attr_dev(svg, "height", "20");
    			attr_dev(svg, "viewBox", "0 0 24 24");
    			add_location(svg, file$4, 13, 5, 367);
    			attr_dev(a0, "href", "https://wpsm.local/");
    			add_location(a0, file$4, 11, 4, 318);
    			attr_dev(a1, "href", "https://wpsm.local/");
    			add_location(a1, file$4, 16, 9, 604);
    			add_location(li0, file$4, 16, 5, 600);
    			attr_dev(a2, "href", "https://wpsm.local/");
    			add_location(a2, file$4, 17, 9, 666);
    			add_location(li1, file$4, 17, 5, 662);
    			attr_dev(ul0, "class", "p-2 bg-base-100 z-10");
    			add_location(ul0, file$4, 15, 4, 561);
    			attr_dev(li2, "tabindex", "0");
    			add_location(li2, file$4, 10, 3, 296);
    			attr_dev(a3, "href", "https://wpsm.local/");
    			add_location(a3, file$4, 20, 7, 740);
    			add_location(li3, file$4, 20, 3, 736);
    			attr_dev(ul1, "class", "menu menu-horizontal p-0");
    			add_location(ul1, file$4, 9, 2, 255);
    			attr_dev(div1, "class", "flex-none");
    			add_location(div1, file$4, 8, 1, 229);
    			attr_dev(div2, "class", "navbar bg-base-100 border border-slate-50 shadow-lg rounded");
    			add_location(div2, file$4, 4, 0, 59);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div0);
    			append_dev(div0, p);
    			append_dev(p, t0);
    			append_dev(div2, t1);
    			append_dev(div2, div1);
    			append_dev(div1, ul1);
    			append_dev(ul1, li2);
    			append_dev(li2, a0);
    			append_dev(a0, t2);
    			append_dev(a0, svg);
    			append_dev(svg, path);
    			append_dev(li2, t3);
    			append_dev(li2, ul0);
    			append_dev(ul0, li0);
    			append_dev(li0, a1);
    			append_dev(ul0, t5);
    			append_dev(ul0, li1);
    			append_dev(li1, a2);
    			append_dev(ul1, t7);
    			append_dev(ul1, li3);
    			append_dev(li3, a3);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*identifier*/ 1) set_data_dev(t0, /*identifier*/ ctx[0]);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Navbar', slots, []);
    	let { identifier = 'X000-Y00000' } = $$props;
    	const writable_props = ['identifier'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Navbar> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('identifier' in $$props) $$invalidate(0, identifier = $$props.identifier);
    	};

    	$$self.$capture_state = () => ({ identifier });

    	$$self.$inject_state = $$props => {
    		if ('identifier' in $$props) $$invalidate(0, identifier = $$props.identifier);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [identifier];
    }

    class Navbar extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, { identifier: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Navbar",
    			options,
    			id: create_fragment$4.name
    		});
    	}

    	get identifier() {
    		throw new Error("<Navbar>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set identifier(value) {
    		throw new Error("<Navbar>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src\components\shared\Button.svelte generated by Svelte v3.49.0 */

    const file$3 = "src\\components\\shared\\Button.svelte";

    // (10:7)    Apply Now  
    function fallback_block$1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Apply Now");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: fallback_block$1.name,
    		type: "fallback",
    		source: "(10:7)    Apply Now  ",
    		ctx
    	});

    	return block;
    }

    function create_fragment$3(ctx) {
    	let button;
    	let button_class_value;
    	let current;
    	const default_slot_template = /*#slots*/ ctx[2].default;
    	const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[1], null);
    	const default_slot_or_fallback = default_slot || fallback_block$1(ctx);

    	const block = {
    		c: function create() {
    			button = element("button");
    			if (default_slot_or_fallback) default_slot_or_fallback.c();
    			attr_dev(button, "class", button_class_value = "btn " + /*className*/ ctx[0]);
    			add_location(button, file$3, 8, 0, 138);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, button, anchor);

    			if (default_slot_or_fallback) {
    				default_slot_or_fallback.m(button, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (default_slot) {
    				if (default_slot.p && (!current || dirty & /*$$scope*/ 2)) {
    					update_slot_base(
    						default_slot,
    						default_slot_template,
    						ctx,
    						/*$$scope*/ ctx[1],
    						!current
    						? get_all_dirty_from_scope(/*$$scope*/ ctx[1])
    						: get_slot_changes(default_slot_template, /*$$scope*/ ctx[1], dirty, null),
    						null
    					);
    				}
    			}

    			if (!current || dirty & /*className*/ 1 && button_class_value !== (button_class_value = "btn " + /*className*/ ctx[0])) {
    				attr_dev(button, "class", button_class_value);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(default_slot_or_fallback, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(default_slot_or_fallback, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(button);
    			if (default_slot_or_fallback) default_slot_or_fallback.d(detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Button', slots, ['default']);
    	let { class: className } = $$props;
    	const writable_props = ['class'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Button> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('class' in $$props) $$invalidate(0, className = $$props.class);
    		if ('$$scope' in $$props) $$invalidate(1, $$scope = $$props.$$scope);
    	};

    	$$self.$capture_state = () => ({ className });

    	$$self.$inject_state = $$props => {
    		if ('className' in $$props) $$invalidate(0, className = $$props.className);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [className, $$scope, slots];
    }

    class Button extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, { class: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Button",
    			options,
    			id: create_fragment$3.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*className*/ ctx[0] === undefined && !('class' in props)) {
    			console.warn("<Button> was created without expected prop 'class'");
    		}
    	}

    	get class() {
    		throw new Error("<Button>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set class(value) {
    		throw new Error("<Button>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src\components\shared\Card.svelte generated by Svelte v3.49.0 */
    const file$2 = "src\\components\\shared\\Card.svelte";
    const get_button_slot_changes = dirty => ({});
    const get_button_slot_context = ctx => ({});
    const get_content_slot_changes = dirty => ({});
    const get_content_slot_context = ctx => ({});
    const get_title_slot_changes = dirty => ({});
    const get_title_slot_context = ctx => ({});

    // (7:21)     
    function fallback_block_2(ctx) {
    	let h2;

    	const block = {
    		c: function create() {
    			h2 = element("h2");
    			h2.textContent = "Card title!";
    			attr_dev(h2, "class", "card-title");
    			add_location(h2, file$2, 7, 3, 179);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, h2, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(h2);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: fallback_block_2.name,
    		type: "fallback",
    		source: "(7:21)     ",
    		ctx
    	});

    	return block;
    }

    // (10:23)     
    function fallback_block_1(ctx) {
    	let p;

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "If a dog chews shoes whose shoes does he choose?";
    			add_location(p, file$2, 10, 3, 256);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: fallback_block_1.name,
    		type: "fallback",
    		source: "(10:23)     ",
    		ctx
    	});

    	return block;
    }

    // (15:4) <Button class="btn-outline btn-secondary">
    function create_default_slot$1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Signup");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot$1.name,
    		type: "slot",
    		source: "(15:4) <Button class=\\\"btn-outline btn-secondary\\\">",
    		ctx
    	});

    	return block;
    }

    // (14:23)      
    function fallback_block(ctx) {
    	let button;
    	let current;

    	button = new Button({
    			props: {
    				class: "btn-outline btn-secondary",
    				$$slots: { default: [create_default_slot$1] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(button.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(button, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const button_changes = {};

    			if (dirty & /*$$scope*/ 2) {
    				button_changes.$$scope = { dirty, ctx };
    			}

    			button.$set(button_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(button.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(button.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(button, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: fallback_block.name,
    		type: "fallback",
    		source: "(14:23)      ",
    		ctx
    	});

    	return block;
    }

    function create_fragment$2(ctx) {
    	let div2;
    	let div1;
    	let t0;
    	let t1;
    	let div0;
    	let current;
    	const title_slot_template = /*#slots*/ ctx[0].title;
    	const title_slot = create_slot(title_slot_template, ctx, /*$$scope*/ ctx[1], get_title_slot_context);
    	const title_slot_or_fallback = title_slot || fallback_block_2(ctx);
    	const content_slot_template = /*#slots*/ ctx[0].content;
    	const content_slot = create_slot(content_slot_template, ctx, /*$$scope*/ ctx[1], get_content_slot_context);
    	const content_slot_or_fallback = content_slot || fallback_block_1(ctx);
    	const button_slot_template = /*#slots*/ ctx[0].button;
    	const button_slot = create_slot(button_slot_template, ctx, /*$$scope*/ ctx[1], get_button_slot_context);
    	const button_slot_or_fallback = button_slot || fallback_block(ctx);

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div1 = element("div");
    			if (title_slot_or_fallback) title_slot_or_fallback.c();
    			t0 = space();
    			if (content_slot_or_fallback) content_slot_or_fallback.c();
    			t1 = space();
    			div0 = element("div");
    			if (button_slot_or_fallback) button_slot_or_fallback.c();
    			attr_dev(div0, "class", "card-actions justify-end");
    			add_location(div0, file$2, 12, 2, 324);
    			attr_dev(div1, "class", "card-body");
    			add_location(div1, file$2, 5, 1, 130);
    			attr_dev(div2, "class", "card w-full bg-base-100 border border-slate-50 shadow-xl");
    			add_location(div2, file$2, 4, 0, 58);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div1);

    			if (title_slot_or_fallback) {
    				title_slot_or_fallback.m(div1, null);
    			}

    			append_dev(div1, t0);

    			if (content_slot_or_fallback) {
    				content_slot_or_fallback.m(div1, null);
    			}

    			append_dev(div1, t1);
    			append_dev(div1, div0);

    			if (button_slot_or_fallback) {
    				button_slot_or_fallback.m(div0, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (title_slot) {
    				if (title_slot.p && (!current || dirty & /*$$scope*/ 2)) {
    					update_slot_base(
    						title_slot,
    						title_slot_template,
    						ctx,
    						/*$$scope*/ ctx[1],
    						!current
    						? get_all_dirty_from_scope(/*$$scope*/ ctx[1])
    						: get_slot_changes(title_slot_template, /*$$scope*/ ctx[1], dirty, get_title_slot_changes),
    						get_title_slot_context
    					);
    				}
    			}

    			if (content_slot) {
    				if (content_slot.p && (!current || dirty & /*$$scope*/ 2)) {
    					update_slot_base(
    						content_slot,
    						content_slot_template,
    						ctx,
    						/*$$scope*/ ctx[1],
    						!current
    						? get_all_dirty_from_scope(/*$$scope*/ ctx[1])
    						: get_slot_changes(content_slot_template, /*$$scope*/ ctx[1], dirty, get_content_slot_changes),
    						get_content_slot_context
    					);
    				}
    			}

    			if (button_slot) {
    				if (button_slot.p && (!current || dirty & /*$$scope*/ 2)) {
    					update_slot_base(
    						button_slot,
    						button_slot_template,
    						ctx,
    						/*$$scope*/ ctx[1],
    						!current
    						? get_all_dirty_from_scope(/*$$scope*/ ctx[1])
    						: get_slot_changes(button_slot_template, /*$$scope*/ ctx[1], dirty, get_button_slot_changes),
    						get_button_slot_context
    					);
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(title_slot_or_fallback, local);
    			transition_in(content_slot_or_fallback, local);
    			transition_in(button_slot_or_fallback, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(title_slot_or_fallback, local);
    			transition_out(content_slot_or_fallback, local);
    			transition_out(button_slot_or_fallback, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			if (title_slot_or_fallback) title_slot_or_fallback.d(detaching);
    			if (content_slot_or_fallback) content_slot_or_fallback.d(detaching);
    			if (button_slot_or_fallback) button_slot_or_fallback.d(detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Card', slots, ['title','content','button']);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Card> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('$$scope' in $$props) $$invalidate(1, $$scope = $$props.$$scope);
    	};

    	$$self.$capture_state = () => ({ Button });
    	return [slots, $$scope];
    }

    class Card extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Card",
    			options,
    			id: create_fragment$2.name
    		});
    	}
    }

    /* src\components\forms\LoginForm.svelte generated by Svelte v3.49.0 */
    const file$1 = "src\\components\\forms\\LoginForm.svelte";

    function create_fragment$1(ctx) {
    	let div7;
    	let form;
    	let div0;
    	let label0;
    	let span0;
    	let t1;
    	let input0;
    	let t2;
    	let div1;
    	let label1;
    	let span1;
    	let t4;
    	let input1;
    	let t5;
    	let div5;
    	let div4;
    	let div2;
    	let button;
    	let t6;
    	let t7;
    	let div3;
    	let input2;
    	let t8;
    	let label2;
    	let t10;
    	let input3;
    	let t11;
    	let div6;
    	let span2;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			div7 = element("div");
    			form = element("form");
    			div0 = element("div");
    			label0 = element("label");
    			span0 = element("span");
    			span0.textContent = "Identifier";
    			t1 = space();
    			input0 = element("input");
    			t2 = space();
    			div1 = element("div");
    			label1 = element("label");
    			span1 = element("span");
    			span1.textContent = "Password";
    			t4 = space();
    			input1 = element("input");
    			t5 = space();
    			div5 = element("div");
    			div4 = element("div");
    			div2 = element("div");
    			button = element("button");
    			t6 = text("Login");
    			t7 = space();
    			div3 = element("div");
    			input2 = element("input");
    			t8 = space();
    			label2 = element("label");
    			label2.textContent = "Remember me";
    			t10 = space();
    			input3 = element("input");
    			t11 = space();
    			div6 = element("div");
    			span2 = element("span");
    			span2.textContent = "Forgot my password";
    			attr_dev(span0, "class", "label-text");
    			add_location(span0, file$1, 61, 4, 1498);
    			attr_dev(label0, "class", "label");
    			attr_dev(label0, "for", "_username");
    			add_location(label0, file$1, 60, 3, 1455);
    			attr_dev(input0, "type", "text");
    			attr_dev(input0, "id", "_username");
    			attr_dev(input0, "class", "input input-bordered w-full");
    			add_location(input0, file$1, 63, 3, 1558);
    			attr_dev(div0, "class", "form-control w-full");
    			add_location(div0, file$1, 59, 2, 1417);
    			attr_dev(span1, "class", "label-text");
    			add_location(span1, file$1, 67, 4, 1757);
    			attr_dev(label1, "class", "label");
    			attr_dev(label1, "for", "_password");
    			add_location(label1, file$1, 66, 3, 1714);
    			attr_dev(input1, "type", "password");
    			attr_dev(input1, "id", "_password");
    			attr_dev(input1, "class", "input input-bordered w-full");
    			add_location(input1, file$1, 69, 3, 1815);
    			attr_dev(div1, "class", "form-control w-full mb-8");
    			add_location(div1, file$1, 65, 2, 1671);
    			attr_dev(button, "class", "btn btn-primary submit");
    			button.disabled = /*disabled*/ ctx[1];
    			add_location(button, file$1, 74, 5, 2052);
    			attr_dev(div2, "class", "basis-1/2");
    			add_location(div2, file$1, 73, 4, 2022);
    			attr_dev(input2, "type", "checkbox");
    			attr_dev(input2, "class", "checkbox checkbox-primary mr-2");
    			attr_dev(input2, "id", "_remember");
    			add_location(input2, file$1, 77, 5, 2231);
    			attr_dev(label2, "for", "_remember");
    			add_location(label2, file$1, 78, 5, 2346);
    			attr_dev(div3, "class", "basis-1/2 flex justify-end items-center");
    			add_location(div3, file$1, 76, 4, 2171);
    			attr_dev(input3, "type", "hidden");
    			attr_dev(input3, "id", "_website");
    			add_location(input3, file$1, 80, 4, 2406);
    			attr_dev(div4, "class", "flex flex-row items-center col1");
    			add_location(div4, file$1, 72, 3, 1971);
    			attr_dev(div5, "class", "form-control mb-4");
    			add_location(div5, file$1, 71, 2, 1935);
    			attr_dev(span2, "class", "text-sm text-right cursor-pointer forgot");
    			add_location(span2, file$1, 84, 3, 2523);
    			attr_dev(div6, "class", "form-control");
    			add_location(div6, file$1, 83, 2, 2492);
    			attr_dev(form, "action", "/");
    			add_location(form, file$1, 58, 1, 1396);
    			attr_dev(div7, "class", "card m-auto p-6 w-2/3 uc-login-form bg-base-100 border border-slate-50 shadow-xl");
    			add_location(div7, file$1, 57, 0, 1298);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div7, anchor);
    			append_dev(div7, form);
    			append_dev(form, div0);
    			append_dev(div0, label0);
    			append_dev(label0, span0);
    			append_dev(div0, t1);
    			append_dev(div0, input0);
    			set_input_value(input0, /*data*/ ctx[0].user_login);
    			append_dev(form, t2);
    			append_dev(form, div1);
    			append_dev(div1, label1);
    			append_dev(label1, span1);
    			append_dev(div1, t4);
    			append_dev(div1, input1);
    			set_input_value(input1, /*data*/ ctx[0].user_password);
    			append_dev(form, t5);
    			append_dev(form, div5);
    			append_dev(div5, div4);
    			append_dev(div4, div2);
    			append_dev(div2, button);
    			append_dev(button, t6);
    			append_dev(div4, t7);
    			append_dev(div4, div3);
    			append_dev(div3, input2);
    			input2.checked = /*data*/ ctx[0].remember;
    			append_dev(div3, t8);
    			append_dev(div3, label2);
    			append_dev(div4, t10);
    			append_dev(div4, input3);
    			set_input_value(input3, /*data*/ ctx[0].website);
    			append_dev(form, t11);
    			append_dev(form, div6);
    			append_dev(div6, span2);

    			if (!mounted) {
    				dispose = [
    					listen_dev(input0, "input", /*input0_input_handler*/ ctx[4]),
    					listen_dev(input1, "input", /*input1_input_handler*/ ctx[5]),
    					listen_dev(button, "click", prevent_default(/*verifyForm*/ ctx[3]), false, true, false),
    					listen_dev(input2, "change", /*input2_change_handler*/ ctx[6]),
    					listen_dev(input3, "input", /*input3_input_handler*/ ctx[7]),
    					listen_dev(span2, "click", /*click_handler*/ ctx[8], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*data*/ 1 && input0.value !== /*data*/ ctx[0].user_login) {
    				set_input_value(input0, /*data*/ ctx[0].user_login);
    			}

    			if (dirty & /*data*/ 1 && input1.value !== /*data*/ ctx[0].user_password) {
    				set_input_value(input1, /*data*/ ctx[0].user_password);
    			}

    			if (dirty & /*disabled*/ 2) {
    				prop_dev(button, "disabled", /*disabled*/ ctx[1]);
    			}

    			if (dirty & /*data*/ 1) {
    				input2.checked = /*data*/ ctx[0].remember;
    			}

    			if (dirty & /*data*/ 1) {
    				set_input_value(input3, /*data*/ ctx[0].website);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div7);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let disabled;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('LoginForm', slots, []);
    	const dispatch = createEventDispatcher();
    	let errorMsg = false;

    	let data = {
    		user_login: '',
    		user_password: '',
    		website: '',
    		remember: false
    	};

    	function submitForm(token) {
    		if (data.website) return;

    		if (data.user_login && data.user_password) {
    			$$invalidate(0, data.token = token, data);

    			fetch(`/wp-json/wpsm/v1/authentication/login`, {
    				method: 'POST',
    				headers: {
    					'Content-Type': 'application/json',
    					'X-WP-Nonce': window?.svelteData?.rest_nonce || '00000000'
    				},
    				body: JSON.stringify(data)
    			}).then(async res => {
    				const { data: _data } = await res.json();

    				if (_data.logged_in) {
    					$$invalidate(0, data.user_login = '', data);
    					$$invalidate(0, data.user_password = '', data);
    					window.location.reload(true);
    				}
    			});
    		}
    	}

    	function verifyForm() {
    		if (typeof window.grecaptcha !== 'undefined') {
    			window.grecaptcha.ready(function () {
    				window.grecaptcha.execute(window.ucOptions.recaptcha_site_key, { action: 'uc_login' }).then(function (token) {
    					submitForm(token);
    				});
    			});
    		} else {
    			// reCaptcha is disabled
    			submitForm(null);
    		}
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<LoginForm> was created with unknown prop '${key}'`);
    	});

    	function input0_input_handler() {
    		data.user_login = this.value;
    		$$invalidate(0, data);
    	}

    	function input1_input_handler() {
    		data.user_password = this.value;
    		$$invalidate(0, data);
    	}

    	function input2_change_handler() {
    		data.remember = this.checked;
    		$$invalidate(0, data);
    	}

    	function input3_input_handler() {
    		data.website = this.value;
    		$$invalidate(0, data);
    	}

    	const click_handler = () => dispatch('forgot', {});

    	$$self.$capture_state = () => ({
    		createEventDispatcher,
    		dispatch,
    		errorMsg,
    		data,
    		submitForm,
    		verifyForm,
    		disabled
    	});

    	$$self.$inject_state = $$props => {
    		if ('errorMsg' in $$props) errorMsg = $$props.errorMsg;
    		if ('data' in $$props) $$invalidate(0, data = $$props.data);
    		if ('disabled' in $$props) $$invalidate(1, disabled = $$props.disabled);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*data*/ 1) {
    			$$invalidate(1, disabled = data.user_login && data.user_password ? '' : 'disabled');
    		}
    	};

    	return [
    		data,
    		disabled,
    		dispatch,
    		verifyForm,
    		input0_input_handler,
    		input1_input_handler,
    		input2_change_handler,
    		input3_input_handler,
    		click_handler
    	];
    }

    class LoginForm extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "LoginForm",
    			options,
    			id: create_fragment$1.name
    		});
    	}
    }

    /* src\components\App.svelte generated by Svelte v3.49.0 */
    const file = "src\\components\\App.svelte";

    // (32:1) {:else}
    function create_else_block(ctx) {
    	let h1;
    	let t1;
    	let loginform;
    	let current;
    	loginform = new LoginForm({ $$inline: true });

    	const block = {
    		c: function create() {
    			h1 = element("h1");
    			h1.textContent = "Login form";
    			t1 = space();
    			create_component(loginform.$$.fragment);
    			add_location(h1, file, 32, 2, 945);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, h1, anchor);
    			insert_dev(target, t1, anchor);
    			mount_component(loginform, target, anchor);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(loginform.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(loginform.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(h1);
    			if (detaching) detach_dev(t1);
    			destroy_component(loginform, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(32:1) {:else}",
    		ctx
    	});

    	return block;
    }

    // (13:1) {#if user && user?.identifier}
    function create_if_block(ctx) {
    	let navbar;
    	let t0;
    	let div1;
    	let div0;
    	let h2;
    	let t2;
    	let div2;
    	let card0;
    	let t3;
    	let card1;
    	let current;
    	navbar = new Navbar({ $$inline: true });

    	card0 = new Card({
    			props: {
    				$$slots: {
    					button: [create_button_slot_1],
    					title: [create_title_slot_1]
    				},
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	card1 = new Card({
    			props: {
    				$$slots: {
    					button: [create_button_slot],
    					title: [create_title_slot]
    				},
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(navbar.$$.fragment);
    			t0 = space();
    			div1 = element("div");
    			div0 = element("div");
    			h2 = element("h2");
    			h2.textContent = "Welcome to our website";
    			t2 = space();
    			div2 = element("div");
    			create_component(card0.$$.fragment);
    			t3 = space();
    			create_component(card1.$$.fragment);
    			attr_dev(h2, "class", "text-2xl font-bold font-sans");
    			add_location(h2, file, 17, 4, 404);
    			attr_dev(div0, "class", "card-body");
    			add_location(div0, file, 16, 3, 376);
    			attr_dev(div1, "class", "card mt-12 w-full bg-base-100 border border-slate-50 shadow-xl");
    			add_location(div1, file, 15, 2, 296);
    			attr_dev(div2, "class", "content mt-12 grid grid-cols-1 md:grid-cols-2 gap-4");
    			add_location(div2, file, 21, 2, 495);
    		},
    		m: function mount(target, anchor) {
    			mount_component(navbar, target, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, div1, anchor);
    			append_dev(div1, div0);
    			append_dev(div0, h2);
    			insert_dev(target, t2, anchor);
    			insert_dev(target, div2, anchor);
    			mount_component(card0, div2, null);
    			append_dev(div2, t3);
    			mount_component(card1, div2, null);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(navbar.$$.fragment, local);
    			transition_in(card0.$$.fragment, local);
    			transition_in(card1.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(navbar.$$.fragment, local);
    			transition_out(card0.$$.fragment, local);
    			transition_out(card1.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(navbar, detaching);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div1);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(div2);
    			destroy_component(card0);
    			destroy_component(card1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(13:1) {#if user && user?.identifier}",
    		ctx
    	});

    	return block;
    }

    // (24:4) 
    function create_title_slot_1(ctx) {
    	let h2;

    	const block = {
    		c: function create() {
    			h2 = element("h2");
    			h2.textContent = "Apply for a scholarship";
    			attr_dev(h2, "slot", "title");
    			attr_dev(h2, "class", "font-sans font-bold text-2xl");
    			add_location(h2, file, 23, 4, 575);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, h2, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(h2);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_title_slot_1.name,
    		type: "slot",
    		source: "(24:4) ",
    		ctx
    	});

    	return block;
    }

    // (25:4) <Button slot="button" class="btn-outline btn-primary">
    function create_default_slot_1(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("Apply");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot_1.name,
    		type: "slot",
    		source: "(25:4) <Button slot=\\\"button\\\" class=\\\"btn-outline btn-primary\\\">",
    		ctx
    	});

    	return block;
    }

    // (25:4) 
    function create_button_slot_1(ctx) {
    	let button;
    	let current;

    	button = new Button({
    			props: {
    				slot: "button",
    				class: "btn-outline btn-primary",
    				$$slots: { default: [create_default_slot_1] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(button.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(button, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const button_changes = {};

    			if (dirty & /*$$scope*/ 2) {
    				button_changes.$$scope = { dirty, ctx };
    			}

    			button.$set(button_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(button.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(button.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(button, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_button_slot_1.name,
    		type: "slot",
    		source: "(25:4) ",
    		ctx
    	});

    	return block;
    }

    // (28:4) 
    function create_title_slot(ctx) {
    	let h2;

    	const block = {
    		c: function create() {
    			h2 = element("h2");
    			h2.textContent = "View previous applications";
    			attr_dev(h2, "slot", "title");
    			attr_dev(h2, "class", "font-sans font-bold text-2xl");
    			add_location(h2, file, 27, 4, 756);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, h2, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(h2);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_title_slot.name,
    		type: "slot",
    		source: "(28:4) ",
    		ctx
    	});

    	return block;
    }

    // (29:4) <Button slot="button" class="btn-outline btn-primary">
    function create_default_slot(ctx) {
    	let t;

    	const block = {
    		c: function create() {
    			t = text("View");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot.name,
    		type: "slot",
    		source: "(29:4) <Button slot=\\\"button\\\" class=\\\"btn-outline btn-primary\\\">",
    		ctx
    	});

    	return block;
    }

    // (29:4) 
    function create_button_slot(ctx) {
    	let button;
    	let current;

    	button = new Button({
    			props: {
    				slot: "button",
    				class: "btn-outline btn-primary",
    				$$slots: { default: [create_default_slot] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(button.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(button, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const button_changes = {};

    			if (dirty & /*$$scope*/ 2) {
    				button_changes.$$scope = { dirty, ctx };
    			}

    			button.$set(button_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(button.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(button.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(button, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_button_slot.name,
    		type: "slot",
    		source: "(29:4) ",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let div;
    	let current_block_type_index;
    	let if_block;
    	let current;
    	const if_block_creators = [create_if_block, create_else_block];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*user*/ ctx[0] && /*user*/ ctx[0]?.identifier) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type(ctx);
    	if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			if_block.c();
    			attr_dev(div, "class", "wrapper");
    			add_location(div, file, 10, 0, 225);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			if_blocks[current_block_type_index].m(div, null);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index !== previous_block_index) {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block = if_blocks[current_block_type_index];

    				if (!if_block) {
    					if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block.c();
    				}

    				transition_in(if_block, 1);
    				if_block.m(div, null);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if_blocks[current_block_type_index].d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('App', slots, []);
    	let { user = {} } = $$props;
    	const writable_props = ['user'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('user' in $$props) $$invalidate(0, user = $$props.user);
    	};

    	$$self.$capture_state = () => ({ Navbar, Card, Button, LoginForm, user });

    	$$self.$inject_state = $$props => {
    		if ('user' in $$props) $$invalidate(0, user = $$props.user);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [user];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, { user: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment.name
    		});
    	}

    	get user() {
    		throw new Error("<App>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set user(value) {
    		throw new Error("<App>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    let wzyTurboJS;
    let anchor = document.getElementById('wzy-turbo-js');

    if ( anchor ) {
    	wzyTurboJS = new App({
    		target: anchor,
    		props: {
    			user: {
    				identifier: '2022-442343'
    			}
    		}
    	});
    }

    var wzyTurboJS$1 = wzyTurboJS;

    return wzyTurboJS$1;

})();
//# sourceMappingURL=wzy-inertia.js.map
