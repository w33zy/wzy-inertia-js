/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
		'./src/**/*.svelte'
	],
  theme: {
    extend: {},
  },
  plugins: [
		require('@tailwindcss/forms'),
		require("daisyui")
	],
	daisyui: {
		themes: ["light"],
	},
	corePlugins: {
		preflight: true,
	}
}
